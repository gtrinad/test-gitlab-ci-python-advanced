from src.main import sum_two

def test_sum_two():
    result = sum_two(1, 2)
    assert result == 3
